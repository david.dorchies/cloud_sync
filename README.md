# Cloud Sync: Enhanced the nextcloud client command line tool

Scripts for using `nextcloudcmd` for syncing local folders with a Nextcloud server.

## Why this tool?

`nextcloudcmd` is suitable when continuous sync is not relevant. Nevertheless,
both command line and GUI nextcloud client are not robust.

I have also given a try to `rclone bisync` (https://rclone.org/commands/rclone_bisync/)
but it's useless since you need to redo the entire synchronisation each time a
conflict appears. Moreover, it is very very slow since it's not multithreaded.

So, I needed a tool for debugging `nextcloudcmd` synchronizations, to be alerted
when the the sync failed and help me to resolve underlying issues.

## Features

- Configuration files for syncing several folders
- Summary the huge log file created by `nextcloudcmd` into a `synth.log` file
- Send a message on Mattermost whenever an sync issue occured

![Mattermost message example](mattermost_msg_example.png)

## Installation

Requirements: R should be installed and the R executable should be in path.

Clone this repository on your hard disk. It's better to clone it in a synced folder
in order to automatically update it on all your machines.

## Use

For one server, set a configuration file in this folder (See `_template.ini`).
Several folders to sync can be defined.

For exemple a `myconfiguration.ini` file as below:

```ini
[cloud_config]
prog=C:\Program Files\Nextcloud\nextcloudcmd.exe
user=write_user_here
pwd=write_password_here
url=https://nextcloud.inrae.fr
[myFolder]
local=C:\MyFolder1
remote=MyDrive/MyFolder1
```

To run sync, type:

```
cloud_sync.bat myconfiguration.ini myFolder
```

## Pitfalls

### Exclamation mark in INI files

In the values of the ini file, one needs to escape  exclamation marks  with `^^^!`

For a password "abcdef!ab123", the syntax is then:

```ini
[cloud_config]
pwd=abcdef^^^!ab123
```
