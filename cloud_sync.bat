@ECHO OFF

SET PATH_BAT=%~dp0
CD /D "%PATH_BAT%"

SET INI_NAME=%1
SET FOLDER=%2
Rscript set_sync.R "%PATH_BAT%%INI_NAME%" "%FOLDER%"
CALL sync.bat
Rscript check_sync.R "%PATH_BAT%%INI_NAME%" "%FOLDER%"
