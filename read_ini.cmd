@setlocal enableextensions enabledelayedexpansion
@echo off
:: $Id$
:: Lecture d'un élément d'un fichier ini (Merci : http://stackoverflow.com/a/2866328 )
::
:: Exemple d'utilisation en ligne de commande :
:: c:\src>type ini.ini
::     [SectionName]
::     total=4
::     [AnotherSectionName]
::     total=7
::     [OtherSectionName]
::     total=12
:: c:\src>read_ini.cmd ini.ini SectionName total
::     4
:: c:\src>read_ini.cmd ini.ini AnotherSectionName total
::     7
:: c:\src>read_ini.cmd ini.ini OtherSectionName total
::     12
::
:: Exemple d'utilisation dans un script :
:: for /f "delims=" %%a in ('call read_ini.cmd ini.ini AnotherSectionName total') do set val=%%a
::
set file=%~1
set area=[%~2]
set key=%~3
set currarea=
for /f "usebackq delims=" %%a in ("!file!") do (
    set ln=%%a
    if "x!ln:~0,1!"=="x[" (
        set currarea=!ln!
    ) else (
        for /f "tokens=1,2 delims==" %%b in ("!ln!") do (
            set currkey=%%b
            set currval=%%c
            if "x!area!"=="x!currarea!" if "x!key!"=="x!currkey!" (
                echo !currval!
            )
        )
    )
)
endlocal